# Install Puppet

cat > /etc/yum.repos.d/puppetlabs.repo << EOM
[puppetlabs-dependencies]
name=puppetlabdsdependencies
baseurl=http://yum.puppetlabs.com/el/6/dependencies/\$basearch
enabled=1
gpgcheck=0

[puppetlabs]
name=puppetlabs
baseurl=http://yum.puppetlabs.com/el/6/products/\$basearch
enabled=1
gpgcheck=0
EOM
yum clean all

# WARNING not sure if ruby-shadow is required 
# 2.7.23 says it wants facter <= 1.2 if facter version not specified
# but puppet by itself says 1.7.1 is ok - going with 1.7.1
yum -y install puppet-2.7.23-1.el6 facter-1.7.1 # facter # ruby-shadow

